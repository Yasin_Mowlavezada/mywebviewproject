package com.pomtech.yasin.mywebbrowser;

import android.content.Intent;
//import android.graphics.Bitmap;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import java.net.URI;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button go , back , refresh , clear , forward;
    EditText url;
    WebView web;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        intialize();

        go.setOnClickListener(this);
        back.setOnClickListener(this);
        refresh.setOnClickListener(this);
        clear.setOnClickListener(this);
        forward.setOnClickListener(this);


    }

    void intialize(){
        go = (Button) findViewById(R.id.goButton);
        back = (Button) findViewById(R.id.backButton);
        forward = (Button) findViewById(R.id.forwardButton);
        refresh = (Button) findViewById(R.id.refreshButton);
        clear = (Button) findViewById(R.id.clearButton);

        url = (EditText) findViewById(R.id.editText);
        web = (WebView) findViewById(R.id.webView);
        web.setWebViewClient(new myWebClient());
        web.getSettings().setJavaScriptEnabled(true);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

//        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void onClick(View v) {

        String urlString = url.getText().toString();

        switch (v.getId()){

            case R.id.goButton : {
                web.loadUrl(urlString);
                progressBar.setVisibility(View.VISIBLE);
//                Uri uri = Uri.parse(urlString);
//                Intent intent = new Intent(Intent.ACTION_VIEW , uri);
//                startActivity(intent);
            } break;

            case  R.id.refreshButton : {
                progressBar.setVisibility(View.VISIBLE);
                web.reload();
            } break;

            case R.id.backButton : {
                if(web.canGoBack()){
                    progressBar.setVisibility(View.VISIBLE);
                    web.goBack();
                }else {

                }
            }

            case R.id.forwardButton : {
                if(web.canGoForward()){
                    progressBar.setVisibility(View.VISIBLE);
                    web.goForward();
                }else{

                }
            } break;

            case  R.id.clearButton : {
                web.clearHistory();
            } break;
        }

    }

    public  class myWebClient extends WebViewClient{
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);


        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            progressBar.setVisibility(View.VISIBLE);
            view.loadUrl(url);
            return false;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);

            progressBar.setVisibility(View.GONE);
        }
    }
}
